<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Origin extends Client {

    public function __construct($environment = 'production', $method = 'GET') {
        parent::__construct($environment, $method);
    }

    public function get($id) {
        $this->setMethod('GET');
        $this->setPath('origin/get/id/' . $id);
        return $this->sendRequest();
    }
    
    public function getByKey($key) {
        $this->setMethod('GET');
        $this->setPath('origin/get/key/' . $key);
        return $this->sendRequest();
    }

}
