<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Token extends Client {

    protected $_client_id;
    protected $_client_key;

    public function __construct($clientId, $clientKey, $environment = 'production', $method = 'GET') {
        parent::__construct($environment, $method);
        $this->setPath('auth/access-token');
        $this->setClientId($clientId);
        $this->setClientKey($clientKey);
    }

    public function setClientId($clientId) {
        $this->_client_id = $clientId;
    }

    public function getClientId() {
        return $this->_client_id;
    }

    public function setClientKey($clientKey) {
        $this->_client_key = $clientKey;
    }

    public function getClientKey() {
        return $this->_client_key;
    }

    public function getToken() {
        $this->setUrl();
        $params = array(
            'client_id' => $this->getClientId(),
            'client_key' => $this->getClientKey()
        );
        $ch = curl_init($this->getUrl());
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        curl_close($ch);
        $response = json_decode($result);
        if (!empty($response->error)) {
            return $response;
        }
        return $response->data;
    }

}
