<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Company extends Client {

    public function __construct($environment = 'production', $method = 'GET') {
        parent::__construct($environment, $method);
    }

    public function search($keyword = null) {
        $this->setMethod('GET');
        if (!empty($keyword)) {
            $this->setPath('company/list/keyword/' . urlencode($keyword));
        } else {
            $this->setPath('company/list');
        }
        return $this->sendRequest();
    }

    public function searchByName($name) {
        $this->setMethod('GET');
        $this->setPath('company/list/keyword/' . urlencode($name));
        return $this->sendRequest();
    }

    public function create($params) {
        $this->setMethod('POST');
        $this->setPath('company/create');
        return $this->sendRequest($params);
    }

}
