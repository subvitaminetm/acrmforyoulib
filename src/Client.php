<?php

/*
 * Acrmforyou Client Request
 */

namespace Acrmforyou;

class Client {

    protected $_environment;
    protected $_domain;
    protected $_method;
    protected $_path;
    protected $_curl_path;
    protected $_url;
    protected $_curl_url;
    protected $_bearer;

    public function __construct($environment, $method) {
        $this->setEnvironment($environment);
        $this->setDomain();
        $this->setMethod($method);
    }

    public function setEnvironment($environment) {
        $this->_environment = $environment;
    }

    public function getEnvironment() {
        return $this->_environment;
    }

    public function setDomain() {
        switch ($this->_environment) {
            case 'development':
                $this->_domain = "http://api.staging.acrmforyou.com/fr/";
                break;
            case 'staging':
                $this->_domain = "http://api.staging.acrmforyou.com/fr/";
                break;
            default:
                $this->_domain = "https://api.acrmforyou.com/fr/";
                break;
        }
    }

    public function getDomain() {
        return $this->_domain;
    }

    public function setMethod($method) {
        $this->_method = $method;
    }

    public function getMethod() {
        return $this->_method;
    }

    public function setPath($path) {
        $this->_path = $path;
    }

    public function getPath() {
        return $this->_path;
    }

    public function setCurlPath($curlPath) {
        $this->_curl_path = $curlPath;
    }

    public function getCurlPath() {
        return $this->_curl_path;
    }

    public function setUrl() {
        $this->_url = $this->getDomain() . $this->getPath();
    }

    public function getUrl() {
        return $this->_url;
    }

    public function setCurlUrl($curlUrl) {
        $this->_curl_url = $curlUrl;
    }

    public function getCurlUrl() {
        return $this->_curl_url;
    }

    public function setToken($token) {
        $this->setBearer($token->access_token);
    }

    public function setBearer($accessToken) {
        $this->_bearer = $accessToken;
    }
    
    public function getBearer() {
        return $this->_bearer;
    }

    public function sendRequest($params = array()) {
        // set url
        $this->setUrl();

        // set headers
        $header = array();
        $header[] = 'Accept: application/json';
        $header[] = 'Content-Type: application/json';
        $header[] = 'Authorization: ' . $this->getBearer();

        $ch = curl_init($this->getUrl());
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        if ($this->getMethod() == "POST") {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($params));
        }

        $result = curl_exec($ch);
        curl_close($ch);

        // return response
        return json_decode($result);
    }

}
