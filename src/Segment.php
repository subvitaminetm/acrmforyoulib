<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Segment extends Client {

    public function __construct($environment = 'production', $method = 'GET') {
        parent::__construct($environment, $method);
    }

    public function search($keyword = null) {
        $this->setMethod('GET');
        if (!empty($keyword)) {
            $this->setPath('segment/list/keyword/' . urlencode($keyword));
        } else {
            $this->setPath('segment/list');
        }
        return $this->sendRequest();
    }

    public function get($id) {
        $this->setMethod('GET');
        $this->setPath('segment/get/id/' . $id);
        return $this->sendRequest();
    }

    public function getAll() {
        $this->setMethod('GET');
        $this->setPath('segment/all');
        return $this->sendRequest();
    }

    public function searchByName($name) {
        $this->setMethod('GET');
        $this->setPath('segment/list/keyword/' . urlencode($name));
        return $this->sendRequest();
    }

    public function create($params) {
        $this->setMethod('POST');
        $this->setPath('segment/create');
        return $this->sendRequest($params);
    }

    public function criterias() {
        $this->setMethod('GET');
        $this->setPath('segment/criterias');
        return $this->sendRequest();
    }
    
    public function calculate($params) {
        $this->setMethod('POST');
        $this->setPath('segment/calculate');
        return $this->sendRequest($params);
    }

}
