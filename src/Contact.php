<?php

namespace Acrmforyou;

use Acrmforyou\Client as Client;

class Contact extends Client {

    public function __construct($environment = 'production', $method = 'GET') {
        parent::__construct($environment, $method);
    }

    public function get($id) {
        $this->setMethod('GET');
        $this->setPath('contact/get/id/' . $id);
        return $this->sendRequest();
    }

    public function search($keyword = null) {
        $this->setMethod('GET');
        if (!empty($keyword)) {
            $this->setPath('contact/list/keyword/' . urlencode($keyword));
        } else {
            $this->setPath('contact/list');
        }
        return $this->sendRequest();
    }

    public function segment($segmentID, $keyword = null, $page = 1, $limit = -1) {
        $this->setMethod('GET');
        if (!empty($keyword)) {
            $this->setPath('contact/segment/id/' . $segmentID . '/page/' . $page . '/limit/' . $limit . '/keyword/' . urlencode($keyword));
        } else {
            $this->setPath('contact/segment/id/' . $segmentID . '/page/' . $page . '/limit/' . $limit);
        }
        return $this->sendRequest();
    }

    public function countBySegment($segmentID) {
        $this->setMethod('GET');
        $this->setPath('contact/segment/id/' . $segmentID . '/count/1');
        return $this->sendRequest();
    }

    public function create($params) {
        $this->setMethod('POST');
        $this->setPath('contact/create');
        return $this->sendRequest($params);
    }

    public function update($params) {
        $this->setMethod('POST');
        $this->setPath('contact/update');
        return $this->sendRequest($params);
    }

}
